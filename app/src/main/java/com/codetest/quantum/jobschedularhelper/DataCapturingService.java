package com.codetest.quantum.jobschedularhelper;

import android.annotation.TargetApi;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;

import com.codetest.quantum.apphelper.CommonUtil;
import com.codetest.quantum.appinstance.QuantumGoutam;
import com.codetest.quantum.fragment.asynchelper.WordInsertAsynctask;
import com.codetest.quantum.fragment.listwordfragment.model.DictionaryItem;
import com.codetest.quantum.fragment.listwordfragment.model.DictionaryResponse;
import com.codetest.quantum.network.RequestType;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class DataCapturingService extends JobService {
    @Override
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public boolean onStartJob(JobParameters params) {
        Log.d("DataCapturingService","Job Service started");
        QuantumGoutam.getInstance().restApi.loadWords().enqueue(new Callback<DictionaryResponse>() {
            @Override
            public void onResponse(Call<DictionaryResponse> call, Response<DictionaryResponse> dictionaryResponse) {
                CommonUtil.printStatus(""+dictionaryResponse.body().toString(),RequestType.LOAD_WORD,dictionaryResponse);
                try {
                    if (dictionaryResponse != null){
                        List<DictionaryItem> listOfDictionary = dictionaryResponse.body().getDictionary();
                        new WordInsertAsynctask(DataCapturingService.this,listOfDictionary).execute();
                    }
                    jobFinished(params,false);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<DictionaryResponse> call, Throwable t) {
                t.printStackTrace();
                Log.e("DataCapturingService","JobServiceError: "+t.getMessage());
            }
        });
        return true;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        return false;
    }

}
