package com.codetest.quantum.apipresenter;

import com.codetest.quantum.fragment.listwordfragment.model.DictionaryResponse;

import retrofit2.Call;
import retrofit2.http.GET;


public interface RestApi {
    @GET("interview/dictionary-v2.json")
    Call<DictionaryResponse> loadWords();
}
