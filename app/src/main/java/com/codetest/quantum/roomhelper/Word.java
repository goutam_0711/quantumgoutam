package com.codetest.quantum.roomhelper;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.provider.BaseColumns;
import android.support.annotation.NonNull;

@Entity(tableName = Word.TABLE_NAME)
public class Word {
    public static final String TABLE_NAME = "word";
    public static final String COLUMN_ID = BaseColumns._ID;
    public static final String COLUMN_NAME_WORD = "word";
    public static final String COLUMN_NAME_FREQ = "freq";

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(index = true, name = COLUMN_ID)
    public int id;
    @ColumnInfo(name = COLUMN_NAME_WORD)
    public String word;
    @ColumnInfo(name = COLUMN_NAME_FREQ)
    public int freq;

    public Word(String word, int freq) {
        this.word = word;
        this.freq = freq;
    }
}
