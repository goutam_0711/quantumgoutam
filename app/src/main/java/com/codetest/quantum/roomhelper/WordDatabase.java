package com.codetest.quantum.roomhelper;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

@Database(entities = { Word.class }, version = 1)
public abstract class WordDatabase extends RoomDatabase {

    private static final String DB_NAME = "words.db";
    private static volatile WordDatabase instance;

    public static synchronized WordDatabase getInstance(Context context) {
        if (instance == null) {
            instance = create(context);
        }
        return instance;
    }

    private static WordDatabase create(final Context context) {
        return Room.databaseBuilder(context, WordDatabase.class, DB_NAME).build();
    }

    public abstract IWordDao getWordDao();
}
