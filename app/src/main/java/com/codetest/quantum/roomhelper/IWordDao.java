package com.codetest.quantum.roomhelper;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;
import android.database.Cursor;

import java.util.List;

@Dao
public interface IWordDao {
    @Query("SELECT * FROM " + Word.TABLE_NAME + " ORDER BY " + Word.COLUMN_NAME_FREQ +" DESC ")
    List<Word> getAllWords();

    @Insert
    long insertWord(Word word);

    @Query("SELECT * FROM " + Word.TABLE_NAME)
    Cursor selectAllWords();

    @Update
    int updateWord(Word word);

    @Query("UPDATE "+ Word.TABLE_NAME + " SET " + Word.COLUMN_NAME_FREQ + " = :freq " + " WHERE " + Word.COLUMN_ID + " = :wordId")
    int updateFreq(int wordId,int freq);

    @Query("SELECT * FROM " + Word.TABLE_NAME + " WHERE UPPER(" + Word.COLUMN_NAME_WORD + ") = UPPER(:word)")
    Cursor selectByWord(String word);

    //@Query("SELECT * FROM " + Word.TABLE_NAME + " WHERE " + Word.COLUMN_NAME_WORD + " = :word")
    @Query("SELECT * FROM " + Word.TABLE_NAME + " WHERE UPPER(" + Word.COLUMN_NAME_WORD + ") = UPPER(:word)")
    List<Word> selectWordByText(String word);
}
