package com.codetest.quantum.fragment.listwordfragment.model;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class DictionaryResponse{

	@SerializedName("dictionary")
	private List<DictionaryItem> dictionary;

	public void setDictionary(List<DictionaryItem> dictionary){
		this.dictionary = dictionary;
	}

	public List<DictionaryItem> getDictionary(){
		return dictionary;
	}

	@Override
 	public String toString(){
		return 
			"DictionaryResponse{" + 
			"dictionary = '" + dictionary + '\'' + 
			"}";
		}
}