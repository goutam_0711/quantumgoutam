package com.codetest.quantum.fragment.listwordfragment.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class DictionaryItem{

	@SerializedName("word")
	private String word;

	@SerializedName("frequency")
	private int frequency;

	public void setWord(String word){
		this.word = word;
	}

	public String getWord(){
		return word;
	}

	public void setFrequency(int frequency){
		this.frequency = frequency;
	}

	public int getFrequency(){
		return frequency;
	}

	@Override
 	public String toString(){
		return 
			"DictionaryItem{" + 
			"word = '" + word + '\'' + 
			",frequency = '" + frequency + '\'' + 
			"}";
		}
}