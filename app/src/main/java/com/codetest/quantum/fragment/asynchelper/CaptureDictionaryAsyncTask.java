package com.codetest.quantum.fragment.asynchelper;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

import com.codetest.quantum.apphelper.CommonUtil;
import com.codetest.quantum.roomhelper.Word;
import com.codetest.quantum.roomhelper.WordDatabase;

import java.util.List;

public class CaptureDictionaryAsyncTask extends AsyncTask<Void,Void,List<Word>> {
    private Context context;
    //private ICaptureDictionaryCallback iCaptureDictionaryCallback;

    public CaptureDictionaryAsyncTask(Context context/*, ICaptureDictionaryCallback iCaptureDictionaryCallback*/) {
        this.context = context;
        //this.iCaptureDictionaryCallback = iCaptureDictionaryCallback;
    }

    @Override
    protected List<Word> doInBackground(Void... voids) {
        List<Word> listOfWords = WordDatabase.getInstance(context).getWordDao().getAllWords();
        //iCaptureDictionaryCallback.onCaptureDictionaryCallback(listOfWords);
        return listOfWords;
    }

    @Override
    protected void onPostExecute(List<Word> words) {
        super.onPostExecute(words);
        Intent intent = new Intent();
        intent.putExtra(CommonUtil.INTENT_WORDS,CommonUtil.toJson(words));
        intent.setAction("com.codetest.quantum.CAPTURED_DATA");
        context.sendBroadcast(intent);
    }
}
