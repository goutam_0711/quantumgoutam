package com.codetest.quantum.fragment.speechfragment;

import android.content.Context;
import android.support.v4.app.FragmentActivity;

public interface ISpeechFragmentPresenter {
    void replaceListWord(Context context,String s);
    void highlightWord(Context context, String text);
    void spokenWordNotFound(Context context, String spokenWord);
}
