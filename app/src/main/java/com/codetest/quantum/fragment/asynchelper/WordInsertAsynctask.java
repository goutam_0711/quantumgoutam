package com.codetest.quantum.fragment.asynchelper;

import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.util.Log;

import com.codetest.quantum.fragment.listwordfragment.model.DictionaryItem;
import com.codetest.quantum.roomhelper.Word;
import com.codetest.quantum.roomhelper.WordDatabase;

import java.util.List;

public class WordInsertAsynctask extends AsyncTask<Void,Void,Void> {

    private List<DictionaryItem> listOfDictionary;
    private Context context;

    public WordInsertAsynctask(Context context, List<DictionaryItem> listOfDictionary) {
        this.listOfDictionary = listOfDictionary;
        this.context = context;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        for (DictionaryItem dictionaryItem : listOfDictionary) {
            String word = dictionaryItem.getWord();
            Cursor c = WordDatabase.getInstance(context).getWordDao().selectByWord(word);
            if (c.getCount() == 0){
                WordDatabase.getInstance(context).getWordDao().insertWord(new Word(word,0));
            }else {
                Log.d("InsertAsync","Word present in DB");
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        new CaptureDictionaryAsyncTask(context).execute();
    }
}
