package com.codetest.quantum.fragment.listwordfragment;

import android.app.Activity;
import android.util.Log;

import com.codetest.quantum.MainActivity;
import com.codetest.quantum.apphelper.CommonUtil;
import com.codetest.quantum.appinstance.QuantumGoutam;
import com.codetest.quantum.fragment.listwordfragment.model.DictionaryResponse;
import com.codetest.quantum.fragment.speechfragment.SpeechFragment;
import com.codetest.quantum.network.RequestType;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ListWordFragmentPresenter implements IListWordFragmentPresenter {
    IListWordFragmentView iListWordFragmentView;

    public ListWordFragmentPresenter(IListWordFragmentView iListWordFragmentView) {
        this.iListWordFragmentView = iListWordFragmentView;
    }

    @Override
    public void replaceSpeechFragment(Activity activity) {
        ((MainActivity) activity).replaceFragment(SpeechFragment.getInstance());
    }

    @Override
    public void loadWordsFromApi() {
        QuantumGoutam.getInstance().restApi.loadWords().enqueue(new Callback<DictionaryResponse>() {
            @Override
            public void onResponse(Call<DictionaryResponse> call, Response<DictionaryResponse> response) {
                CommonUtil.printStatus(""+response.body().toString(),RequestType.LOAD_WORD,response);
                iListWordFragmentView.onWordResponse(response.body());
            }

            @Override
            public void onFailure(Call<DictionaryResponse> call, Throwable t) {
                iListWordFragmentView.onFailResponse("Some Error found. Please try again later.");
            }
        });
    }
}
