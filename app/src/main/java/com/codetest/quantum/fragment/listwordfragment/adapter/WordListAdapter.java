package com.codetest.quantum.fragment.listwordfragment.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.codetest.quantum.R;
import com.codetest.quantum.roomhelper.Word;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WordListAdapter extends RecyclerView.Adapter<WordListAdapter.WordHolder> {

    private Context context;
    private List<Word> wordList;
    private String spokenWord;

    public WordListAdapter(Context context, List<Word> wordList, String spokenWord) {
        this.context = context;
        this.wordList = wordList;
        this.spokenWord = spokenWord;
    }

    @NonNull
    @Override
    public WordHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new WordHolder(LayoutInflater.from(context).inflate(R.layout.item_word, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull WordHolder wordHolder, int i) {
        wordHolder.tvWord.setText(wordList.get(i).word);
        wordHolder.tvFrequency.setText(String.valueOf(wordList.get(i).freq));
        if (spokenWord != null){
            if (wordList.get(i).word.equalsIgnoreCase(spokenWord)){
                wordHolder.tvWord.setTextColor(ContextCompat.getColor(context,android.R.color.holo_red_dark));
                wordHolder.tvFrequency.setTextColor(ContextCompat.getColor(context,android.R.color.holo_red_dark));
            }
        }
    }

    @Override
    public int getItemCount() {
        return wordList.size();
    }

    public class WordHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvWord)
        TextView tvWord;
        @BindView(R.id.tvFrequency)
        TextView tvFrequency;
        public WordHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
