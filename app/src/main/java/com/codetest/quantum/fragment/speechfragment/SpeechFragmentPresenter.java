package com.codetest.quantum.fragment.speechfragment;

import android.content.Context;

import com.codetest.quantum.MainActivity;
import com.codetest.quantum.fragment.listwordfragment.ListWordFragment;

public class SpeechFragmentPresenter implements ISpeechFragmentPresenter {

    @Override
    public void replaceListWord(Context context,String resString) {
        ((MainActivity)context).replaceFragment(ListWordFragment.getInstance());
    }

    @Override
    public void highlightWord(Context context, String text) {
        ((MainActivity)context).highlightWord(ListWordFragment.getInstance(),text);
    }

    @Override
    public void spokenWordNotFound(Context context, String spokenWord) {
        ((MainActivity)context).spokenWordNotFound(context,spokenWord);
    }
}
