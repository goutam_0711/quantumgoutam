package com.codetest.quantum.fragment.speechfragment;

public interface IWordFoundCallback {
    void onWordFound(String word);

    void onWordNotFound(String spokenWord);
}
