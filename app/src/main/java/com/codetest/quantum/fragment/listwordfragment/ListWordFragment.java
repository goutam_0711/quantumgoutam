package com.codetest.quantum.fragment.listwordfragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.codetest.quantum.MainActivity;
import com.codetest.quantum.R;
import com.codetest.quantum.apphelper.CommonUtil;
import com.codetest.quantum.broadcasthelper.DataCapturedReceiver;
import com.codetest.quantum.fragment.asynchelper.CaptureDictionaryAsyncTask;
import com.codetest.quantum.fragment.asynchelper.WordInsertAsynctask;
import com.codetest.quantum.fragment.listwordfragment.adapter.WordListAdapter;
import com.codetest.quantum.fragment.listwordfragment.model.DictionaryItem;
import com.codetest.quantum.fragment.listwordfragment.model.DictionaryResponse;
import com.codetest.quantum.network.AppNetworkChecker;
import com.codetest.quantum.roomhelper.Word;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class ListWordFragment extends Fragment implements IListWordFragmentView ,DataCapturedReceiver.ICaptureDictionaryCallback{
    @BindView(R.id.btnSpeak)
    Button btnSpeak;
    @BindView(R.id.rcvWordList)
    RecyclerView rcvWordList;
    Unbinder unbinder;
    ListWordFragmentPresenter iListWordFragmentPresenter;
    private static DataCapturedReceiver.ICaptureDictionaryCallback iCaptureDictionaryCallback;
    Bundle mBundle;
    public static ListWordFragment getInstance() {
        return new ListWordFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list_word, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        iListWordFragmentPresenter = new ListWordFragmentPresenter(this);
        if (AppNetworkChecker.isNetworkConnected(getActivity())){
            //iListWordFragmentPresenter.loadWordsFromApi();
        }
        btnSpeak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iListWordFragmentPresenter.replaceSpeechFragment(getActivity());
            }
        });
        mBundle = getArguments();
        iCaptureDictionaryCallback = this;
        new DataCapturedReceiver().registerCallback(iCaptureDictionaryCallback);
        if (mBundle!=null){
            new CaptureDictionaryAsyncTask(getActivity()).execute();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onWordResponse(DictionaryResponse dictionaryResponse) {
        if (dictionaryResponse != null){
            List<DictionaryItem> listOfDictionary = dictionaryResponse.getDictionary();
            //new WordInsertAsynctask(getActivity(),listOfDictionary).execute();
        }
    }

    @Override
    public void onFailResponse(String error) {
        CommonUtil.showToast(getActivity(),error);
    }

    @Override
    public void onCaptureDictionaryCallback(List<Word> listOfWords) {
        if (mBundle == null){
            if (listOfWords.size() > 0){
                rcvWordList.setLayoutManager(new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL,false));
                WordListAdapter wordListAdapter = new WordListAdapter(getActivity(),listOfWords,null);
                rcvWordList.setAdapter(wordListAdapter);
            }
        }else {
            if (listOfWords.size() > 0){
                String spokenWord = mBundle.getString(MainActivity.SPOKEN_WORD);
                rcvWordList.setLayoutManager(new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL,false));
                WordListAdapter wordListAdapter = new WordListAdapter(getActivity(),listOfWords,spokenWord);
                rcvWordList.setAdapter(wordListAdapter);
            }
        }
    }
}
