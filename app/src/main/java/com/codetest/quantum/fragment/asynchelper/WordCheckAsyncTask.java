package com.codetest.quantum.fragment.asynchelper;

import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;

import com.codetest.quantum.apphelper.CommonUtil;
import com.codetest.quantum.fragment.speechfragment.IWordFoundCallback;
import com.codetest.quantum.roomhelper.Word;
import com.codetest.quantum.roomhelper.WordDatabase;

import java.util.List;

public class WordCheckAsyncTask extends AsyncTask<Void,Void,Void> {
    private Context context;
    private String spokenWord;
    private IWordFoundCallback iWordFoundCallback;

    public WordCheckAsyncTask(Context context, String spokenWord, IWordFoundCallback iWordFoundCallback) {
        this.context = context;
        this.spokenWord = spokenWord;
        this.iWordFoundCallback = iWordFoundCallback;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        List<Word> listOrResult = WordDatabase.getInstance(context).getWordDao().selectWordByText(spokenWord);
        if (listOrResult.size() > 0){
            int id = listOrResult.get(0).id;
            int freq = listOrResult.get(0).freq;
            String word = listOrResult.get(0).word;
            WordDatabase.getInstance(context).getWordDao().updateFreq(id,freq+1);
            iWordFoundCallback.onWordFound(word);
        }else {
            iWordFoundCallback.onWordNotFound(spokenWord);

        }
        return null;
    }
}
