package com.codetest.quantum.fragment.listwordfragment;

import android.app.Activity;

public interface IListWordFragmentPresenter {
    void replaceSpeechFragment(Activity activity);
    void loadWordsFromApi();

}
