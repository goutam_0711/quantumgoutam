package com.codetest.quantum.fragment.listwordfragment;

import com.codetest.quantum.fragment.listwordfragment.model.DictionaryResponse;

public interface IListWordFragmentView {
    void onWordResponse(DictionaryResponse dictionaryResponse);
    void onFailResponse(String error);
}
