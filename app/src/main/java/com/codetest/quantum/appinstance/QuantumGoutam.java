package com.codetest.quantum.appinstance;

import android.content.Context;
import android.support.multidex.MultiDexApplication;

import com.codetest.quantum.apipresenter.RestApi;
import com.codetest.quantum.network.ApiConstants;
import com.splunk.mint.Mint;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class QuantumGoutam extends MultiDexApplication {
    private static QuantumGoutam mInstance = null;
    public static Context context;
    public RestApi restApi;

    @Override
    public void onCreate() {
        super.onCreate();
        Mint.initAndStartSession(this, "aee9e9e9");
        mInstance = this;
        context = this;
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(20000, TimeUnit.MILLISECONDS)
                .readTimeout(30000, TimeUnit.MILLISECONDS)
                .writeTimeout(15000, TimeUnit.MILLISECONDS)
                .build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiConstants.BASEURL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        restApi = retrofit.create(RestApi.class);
    }

    public static synchronized QuantumGoutam getInstance() {
        return mInstance;
    }
}
