package com.codetest.quantum;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.widget.FrameLayout;

import com.codetest.quantum.apphelper.CommonUtil;
import com.codetest.quantum.fragment.listwordfragment.ListWordFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {
    public static final String SPOKEN_WORD = "SPOKEN_WORD";
    @BindView(R.id.frmMain)
    FrameLayout frmMain;
    private FragmentManager mFragmentManager;
    private FragmentTransaction mFragmentTransaction;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        //start job service to capture data
        CommonUtil.scheduleJobForDataCapture(MainActivity.this);
        mFragmentManager = getSupportFragmentManager();
        addFragment(ListWordFragment.getInstance());
    }

    public void addFragment(Fragment fragment){
        mFragmentTransaction = mFragmentManager.beginTransaction();
        mFragmentTransaction.add(R.id.frmMain, fragment);
        mFragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        mFragmentTransaction.commit();
    }

    public void replaceFragment(Fragment fragment){
        mFragmentTransaction = mFragmentManager.beginTransaction();
        mFragmentTransaction.replace(R.id.frmMain, fragment);
        mFragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        mFragmentTransaction.commit();
    }


    public void highlightWord(Fragment fragment, String text) {
        Bundle bundle = new Bundle();
        bundle.putString(MainActivity.SPOKEN_WORD,text);
        fragment.setArguments(bundle);
        mFragmentTransaction = mFragmentManager.beginTransaction();
        mFragmentTransaction.replace(R.id.frmMain, fragment);
        mFragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        mFragmentTransaction.commit();
    }

    public void spokenWordNotFound(Context context, String spokenWord) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                CommonUtil.showToast(context,spokenWord+" : is not found in the dictionary.");
            }
        });
    }
}
