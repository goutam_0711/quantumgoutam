package com.codetest.quantum.broadcasthelper;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.codetest.quantum.apphelper.CommonUtil;
import com.codetest.quantum.roomhelper.Word;

import java.util.List;

public class DataCapturedReceiver extends BroadcastReceiver {
    private static ICaptureDictionaryCallback iCaptureDictionaryCallback;

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getStringExtra(CommonUtil.INTENT_WORDS) != null){
            List<Word> listOfWord = CommonUtil.fromJson(intent.getStringExtra(CommonUtil.INTENT_WORDS));
            if (iCaptureDictionaryCallback != null)
                iCaptureDictionaryCallback.onCaptureDictionaryCallback(listOfWord);
        }

    }

    public interface ICaptureDictionaryCallback {
        void onCaptureDictionaryCallback(List<Word> listOfWords);
    }

    public void registerCallback(ICaptureDictionaryCallback iCaptureDictionaryCallback) {
        this.iCaptureDictionaryCallback = iCaptureDictionaryCallback;
    }
}
